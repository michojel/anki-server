#!/usr/bin/env bash

set -euo pipefail

# shellcheck disable=SC1091
source /opt/anki/bin/activate

# shellcheck disable=SC2046
exec env $(cat "$SYNC_USERS") python -m anki.syncserver
