ARG UBUNTU_RELEASE=23.04
FROM ubuntu:${UBUNTU_RELEASE}

ARG VERSION=23.10.1
ARG PORT=80
ARG UID=62384
ARG GID

# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        python3 \
        python3-venv && \
    apt-get clean all && \
    rm -rf /var/lib/apt/lists/*

SHELL ["/bin/bash", "-c"]
# hadolint ignore=DL3013,SC1091
RUN python3 -m venv /opt/anki && \
    . /opt/anki/bin/activate && \
    if [[ "${VERSION:-latest}" = "latest" ]]; then \
        pip install --no-cache-dir anki; \
    elif [[ "${VERSION}" =~ ^v?([[:digit:]]+\..*) ]]; then \
        pip install --no-cache-dir "anki == ${BASH_REMATCH[1]}"; \
    else \
        printf 'Ignoring VERSION=%s\n' "$VERSION" >&2; \
        pip install --no-cache-dir anki; \
    fi

RUN groupadd --system --gid "${GID:-$UID}" ankisyncd && \
    useradd --system -g ankisyncd --uid "${UID}" ankisyncd

RUN mkdir -p /var/lib/anki/syncserver && \
    chown ankisyncd:ankisyncd -R /var/lib/anki

COPY ./entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod 755 /usr/local/bin/entrypoint.sh

USER ankisyncd:ankisyncd

VOLUME /var/lib/anki/syncserver

EXPOSE ${PORT}/tcp

ENV SYNC_PORT=${PORT}
ENV SYNC_BASE=/var/lib/anki/syncserver
ENV SYNC_USERS=/var/lib/anki/syncusers

WORKDIR /var/lib/anki/syncserver

LABEL org.opencontainers.image.authors="Michal Minář <mm@michojel.cz>" \
      org.opencontainers.image.base.name="docker.io/ubuntu:${UBUNTU_RELEASE}" \
      org.opencontainers.image.documentation="https://gitlab.com/michojel/anki-server/" \
      org.opencontainers.image.licenses="AGPL-3.0" \
      org.opencontainers.image.source="https://gitlab.com/michojel/anki-server/" \
      org.opencontainers.image.title="Anki Sync Server" \
      org.opencontainers.image.version="${VERSION}"

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

# ex: et ts=4 sw=4 :
