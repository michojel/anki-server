# Anki Sync Server

Anki container image suitable to run self-hosted sync server. See [upstream
documentation](https://docs.ankiweb.net/sync-server.html) for more details.

## Usage

### NixOS

Check out [this example if you use
NixOS](https://github.com/michojel/NixOS/blob/master/michowps/anki-syncserver.nix).

### As a systemd service on another Linux distribution

1. Create user and group:

    ```sh
    sudo groupadd --system --gid 62384 ankisyncd
    sudo useradd --system -g ankisyncd --uid 62384 ankisyncd
    ```

1. Create a hosting directory:

    ```sh
    sudo mkdir -p -m 700              /var/lib/anki/syncserver
    sudo chown -R ankisyncd:ankisyncd /var/lib/anki
    ```

1. Create a set of users (assuming bash):

    ```sh
    printf '%s\n' SYNC_USER1=bob:strong-password SYNC_USER2=alice:PASSWORD | \
        sudo tee /var/lib/anki/syncusers
    sudo chmod 600 /var/lib/anki/syncusers
    sudo chown ankisyncd:ankisyncd /var/lib/anki/syncusers
    ```

1. Create a systemd service:

    ```sh
    sudo cp config/systemd/anki-syncserver.service \
        /etc/systemd/system/anki-syncserver.service
    ```

1. Enable systemd service:

    ```sh
    sudo systemctl daemon-reload
    sudo systemctl enable --now anki-syncserver.service
    ```

1. Deploy a TLS secured HTTP reversed proxy in front. This is out of scope of
   this README. Check out [this
   example](https://github.com/ankicommunity/anki-sync-server#installing).
